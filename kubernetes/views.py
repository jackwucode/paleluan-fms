# -*- coding: UTF-8 -*-
import requests
from django.shortcuts import render_to_response, get_object_or_404, render
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import permission_required, login_required

PAGE_SIZE = 10  # 每页显示条数
current_page_total = 10  # 分页下标


@login_required
def index(request):
    user = request.user
    if user.is_superuser:
        role = '超级管理员'
    elif user.is_anonymous():
        role = '匿名用户'
    else:
        role = '普通用户'
    request.role = role
    return render_to_response('base/index.html', {'request': request})

# def time_count(content, start_time, end_time):
#
#     start_time = time.strptime(str(start_time).split('+')[0], "%Y-%m-%d %H:%M:%S")
#     end_time = time.strptime(
#         str(end_time).split('+')[0], "%Y-%m-%d %H:%M:%S")
#     timestamp = int(time.mktime(end_time)) - int(time.mktime(start_time))
#
#     setattr(content, 'time', str(timestamp // 3600) + '小时' + str(timestamp % 3600 // 60) + '分')

@login_required
@permission_required('kubernetes.view_container', raise_exception=True)
def container_list(request):

    pods = requests.get('http://192.168.201.56:8080/api/v1/pods')
    pods_dict = pods.json()
    # print(pods_dict['items'][0])
    containerList = []
    for i in pods_dict['items']:
        for v in i['spec']['containers']:
            namespace = i['metadata']['namespace']
            nodeName = i['spec']['nodeName']
            status = i['status']['phase']
            podIP = i['status']['podIP']
            startTime = i['status']['startTime']
            containerName = v['name']
            image = v['image']
            containerImage = image.split('/')[-1]
            # print('%s:%s' % (containerName, image))
            # print(namespace, nodeName, status, podIP, startTime)
            # print("------------------------------------------------------")
            data = {'containerName': containerName, 'podIP': podIP,
                    'namespace': namespace, 'containerImage': containerImage,
                    'nodeName': nodeName, 'status': status, 'startTime': startTime}
            containerList.append(data)

    request.breadcrumbs((('首页', '/'), ('容器列表', reverse('container_list'))))

    return render_to_response('kubernetes/container.html', {'request': request, 'containerList': containerList})
